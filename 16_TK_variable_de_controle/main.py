#coding:utf-8
"""
    variable de contrôle en Python
        - stringVar: chaîne de caractère (str)
        - intVar: nombre entier (int)
        - doubleVar: nombre flottants (float, double)
        - booleanVar: booléen (bool)
"""

import tkinter
"""
    a pour role d'assigner la valeur saisie dans le champ texte au label
"""
# Mise en place d'un observateur:
def updateLabel(*args):
    labelVar.set(entryVar.get())



# main window

app=tkinter.Tk()
app.geometry("800x600")
app.title("variables tkinter")

# widgets

# > Création d'une variable
labelVar = tkinter.StringVar()

entryVar = tkinter.StringVar()
# 'w' appel l'observateur  quand la variable sera modifiée (written)
# autres modes:     'r': appel de l'observateur quand la variable sera lue (read)
#                   'u': à la suppresion de la variable (undifined)
entryVar.trace('w', updateLabel)

# modification de la variable de contrôle avec la fonction set
labelVar.set("Label ...")


# Récupération de la variable avec 'get'
print("Label: ", labelVar.get())

# connecter (associer) la variable au label 'text variable'

label = tkinter.Label(app, text="Bonjour !", textvariable=labelVar)
entry = tkinter.Entry(app, textvariable=entryVar)

#===========================================================#
#===============Autres exemples: radioButton================#
#===========================================================#

# Observer
def upDateObserver(*args):
    if genderVar.get():
        print("c'est un Homme")
        labelVar.set("c'est un Homme")
    else:
        print("c'est une Femme")
        labelVar.set("c'est une Femme")

# Track des boutons
## défintion de la variable de contrôle pour les 2 boutons radio
genderVar=tkinter.IntVar()

## track des boutons
genderVar.trace('w', upDateObserver)

# Widget + Mise en place de la liaison entre les boutons et la var de ctrl avec l'option 'variable'
radioBtn1=tkinter.Radiobutton(app, text="Homme", value=1,variable=genderVar)
radioBtn2=tkinter.Radiobutton(app, text="Femme", value=0,variable=genderVar)

#===========================================================#
#=================Positionnement et boucle==================#
#===========================================================#

# positionnement
label.pack()
entry.pack()
radioBtn1.pack()
radioBtn2.pack()

# boucle
app.mainloop()