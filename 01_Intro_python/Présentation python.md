# Introduction

**Installer python :**
https://www.python.org/
Download / Windows/ Python version x86 (pour 64bits)

**A propos: **
Extension des fichiers Python : `.py`
Implémentation de référence :
    - CPython : écrite en C 
    - Pypy : écrite entièrement en Python
    - JPython : réalisé en java
    - IronPython : plate-forme .NET

Type de modules: 
        - module de base: fichier python (.py)
        - module de haut niveau: repertoire
