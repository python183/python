#coding:utf-8

import tkinter

"""
    En plus de add_command():
                                - add_checkbutton
                                - add_radiobutton
                                - add_separator
"""

# Main window
app = tkinter.Tk()
app.geometry("800x600")
app.title("Création de menu")

# Widgets
# 1. Création d'un menu [menu = barre de menu + sous menu]
# Création de la barre de menu
mainMenu=tkinter.Menu(app)

# Définition de la méthode showAbout
def showAbout():
    # Création de la sous fenêtre avec la fonction TopLevel
    aboutWindow=tkinter.Toplevel()

    #paramétrage de la sous fenêtre
    aboutWindow.title("A propos")
    lbInfos=tkinter.Label(aboutWindow, text="Info: ")

    #Positionnement de la sous-fenêtre
    lbInfos.pack()

# 3. 1er menu
menu1=tkinter.Menu(mainMenu, tearoff=0)
    #3.2 Ajout des commandes
menu1.add_command(label="M1.Option1")
menu1.add_command(label="M1.Option2")
menu1.add_radiobutton(label="radioButton1",value=0)
menu1.add_radiobutton(label="radioButton2",value=1)
menu1.add_separator()
menu1.add_command(label="Quitter", command=app.quit)
menu1.add_command(label="About", command=showAbout)

# 4. 2nd menu
    #4.2 Ajout des commandes
menu2=tkinter.Menu(mainMenu, tearoff=0)
menu2.add_command(label="M2.Option1")
menu2.add_checkbutton(label="Check Button")
menu2.add_command(label="M2.Option2")
menu2.add_command(label="M2.Option3")

    #4.3 Sous-sous menu
sousMenu2=tkinter.Menu(menu2, tearoff=0)
menu2.add_cascade(label="M2.Option4",menu=sousMenu2)
sousMenu2.add_command(label="M2.SM2.Option1")

# 5. Ajout des menus à la barre
mainMenu.add_cascade(label="First", menu=menu1)
mainMenu.add_cascade(label="Second", menu=menu2)

# 2. Configuration de la barre de menu (attacher la barre à la fenêtre principale)
app.config(menu=mainMenu)






# Boucle
app.mainloop()