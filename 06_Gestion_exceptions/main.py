#coding:utf-8
"""
    Exemple d'exception python
        - ValueError
        - typeError
        - ZeroDivisionError
        - osError
        - nameError
        - AssertionError
        - ... ect
"""

# programme qui va provoquer une exception
ageUser=input("Quel es ton âge ?")
ageUser=int(ageUser)

print("\t tu as: {} ans".format(ageUser))

# > Saisie d'un String : une exception est levée de type : ValueError -> provoquée par le cast

# > Gestion de l'exeption avec try... except (minimum)
print("\n\tGestion de l'exception: \n")
ageUser=input("Quel es ton âge ?")
try:
    # ligne qui peut provoquer une exception
    ageUser=int(ageUser)
except:
    print("ERROR: l'âge est incorrect !")

else:
    print("\t tu as: ",ageUser,"ans")

finally:
    print("FIN DU PROGRAMME...") 

"""
    - cas de plusieurs exceptions levées
        -> Spécifier les exceptions dans des blocs 'except'
"""

print("\n\t traitement de plusieurs exceptions\n")
nbr1=150
nbr2=input("choisir le nombre pour diviser: ")

try:
    nbr2=int(nbr2) # ValueError
    print("Résultat = {}".format(nbr1/nbr2)) # ZeroDivisionError

except ZeroDivisionError:
    print("Vous ne pouvez pas diviser par zéro")

except ValueError:
    print("veuiller entrer un nombre")

except:
    print ("Valeur incorrecte")

else:
    print("We did it !")

finally:
    print("FIN DU PROGRAMME...")


 # > lever une exception avec le mot : raise (eq throw) 


print("\n\tlever une exception:\n")
try:
    age=input("Quel age as-tu ?")
    age=int(age)
    if age < 18:
        raise ValueError("erreur")
except ValueError:
    print("DEGAGE MINO !")
    
"""
    - gestion des assertions:
        -> test de code avec le mot: assert -> levée d'exception: AssertionError
"""

print("\n\tGestion des assertion:\n")
try:
    age = input("Quel âge as-tu ?")
    age = int(age)

    # l'âge doit être < 25 --> levée d'exception de type AssertionError

    assert age < 25
except AssertionError:
    print("l'âge doit être < 25")