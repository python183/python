#coding:utf-8

"""
    - un dictionnaire est une collection non ordonnée de relation entre clef/valeur
        -> les clef sont uniques
"""

print("\n\tCréation d'un dico\n")
# > Création d'un dictionnaire: {clef:valeur, clef:valeur,...}
dico = {} # Déclaration d'un dictionnaire vide
dico = {1:45,"prenom":"Jean-Claude","chien":"mammifère, le meilleur ami de l'homme"}

# > Accéder à un élément dans un dictionnaire: <nom_dico>[<nom_clef>]
print("\n\tAccès à un élément\n")
print(dico["prenom"])
print(dico["chien"])

print("\n\tModification d'un élément\n")
# > Ajouter et modifier un élément
print(dico)
dico["chat"]="connard"
print(dico)

# NB: Si la clef existe > ça sera une modification
dico["chat"]="gros connard"
print(dico)

print("\n\tSupression à un élément\n")
####### > Supprimer un élément
# Création
print("\tAvec pop")
dico={"chat":"felin","chien":"cannidé"}
# Suppression avec 'pop(<clef>)
print(dico)
valeurSupprimer=dico.pop("chat")
print(dico)
print("valeur supprimée: ",valeurSupprimer)
# Suppression avec del

print("\tAvec del")
del dico["chien"]
print(dico)

####### > Parcours d'un dictionnaire
print("\n\tParcours d'un dictionnaire\n")
print("\nLes clefs: ")
# Les clefs
dico = {1:45,"prenom":"Jean-Claude","chien":"mammifère, le meilleur ami de l'homme"}
for key in dico.keys():
    print("\t- ",key)

print("\nLes clefs: ")
# Les valeurs
for value in dico.values():
    print("\t- ",value)

print("\nclefs/valeurs: ")
for k,v in dico.items():
    print("- Clef: {} - Valeur: {}".format(k,v))

# Ajouter un tuple en tant que valeur
dico[(2,3)]='ok'

#Exception, KeyError: (clef introuvable)
# print(dico["ok"]) # Déclencher un KeyError
print(dico)

try:
        print(dico["ok"])
except KeyError:
    print("la clef n'existe pas")

# Les paramètres nommés dans une fonctions
# > fonctions avec paramètres
def afficher(nom,age):
    print(nom,age)
afficher ("toto",24)

# > fonction avec paramètre dico (*= nom de la variable | paramètres nommés)
def afficher (**params):
    print(params)

afficher(p=145, nom="toto", age=24
)

# > autre type de collection: ensemble (Set en java)
# > un ensemble est une collection non ordonnée d'objets unique
# > types:  - Set (ensemble modifiable)
#           - frozenSet (ensemble non modifiable)

