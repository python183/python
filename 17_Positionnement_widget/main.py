#coding:utf-8

import tkinter

# Main window
app = tkinter.Tk()
app.geometry("800x600")
app.title("Positionnement widget")

# Widgets 
# création d'un cadre (frame): conteneur de widget
# NB: intérêt des cadres (frame): gestion des widgets en groupe
mainFrame=tkinter.Frame(app, width=300, height=200, borderwidth=2, bg="lightblue")

# Création d'un cadre nommé: text = un label
mainLabelFrame = tkinter.LabelFrame(app, text="cadre1",width=300, height=200, borderwidth=2, bg="cyan")


# création d'un bouton: widget parent -> frame
btn = tkinter.Button(app, text="Welcome")
label = tkinter.Label(app, text="Hello")
entry = tkinter.Entry(app)




# positionnement
"""
    option pack()
        - side: top, bottom, left and right
        - fill (remplir): 'x' ou 'y'
        - padx | pady: les marges extérieurs (en dehors du widget)
        - ipadx | ipady: les marges interieur (en dehors du widget)
        - grid

"""

    # par pack()
"""
mainFrame.pack()
mainLabelFrame.pack()
label.pack(side="top")
entry.pack (side="top")
btn.pack(side="bottom",fill="x")
"""

    # par grille: grid()
# Eviter de mélanger pack et grid
# NB: recouvrement de ligne: rowspan, de colone: columnspan (faire occuper un élément sur plusieurs lignes ou colones)    
# Gestion des marges (idem que pack, padx et pady...)
# Gestion des marges: sticky(jouer sur les orientations)
#   -> orientation: 'n', 's', 'e', 'w' (nord, sud, east, west)

"""
label.grid(row=0,column=1, columnspan=2)
btn.grid(padx=10,pady=10,sticky='se')
entry.grid(row=0,column=3)
"""
# Positionnement par place: place() [placer un widget au pixel]

label.pack()
btn.pack()
entry.pack()


# Boucle
app.mainloop()

