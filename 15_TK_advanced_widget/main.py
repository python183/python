#coding:utf-8

import tkinter
from tkinter import messagebox

# > fenêtre principale
app=tkinter.Tk()

# Taille de la fenêtre pardéfaut
app.geometry("800x600")

# > widget: case à cocher: checkbutton
case=tkinter.Checkbutton(app, text="publier ?",offvalue=2,onvalue=5)
# NB: case à cocher: valeur=1 sinon 0
# changement - options: offValue () et onValue (case coché)

# > widget: les bouton radio: radioButton
# on coche une seul des deux

bouton1 = tkinter.Radiobutton(app,text="Homme",value=1)
bouton2 = tkinter.Radiobutton(app,text="Femme",value=2)
#NB: Les deux boutons sont pré cochés
# pour pouvoir cocher l'une des deux -> value (valeur différentes)


# widget pour les curseurs (0-100)
# changer l'échelle des valeurs: 'from_' 'to'
# changer le step avec indicateur: tkintervalle
curseur=tkinter.Scale(app, from_=0,to=100,tickinterval=10, orient='horizontal', label='puissance',length=1000)

# widget spinbox
spinBox=tkinter.Spinbox(app, from_=1,to=20)

# widget: listbox
lb=tkinter.Listbox(app)
lb.insert(1,"Windows")
lb.insert(2,"GNU/Linux")
lb.insert(2,"MacOS")

#======================================================================================#
#=============Fenêtre modale (pop-up) | message d'erreur, avertissement ===============#
#======================================================================================#
"""
    Fonction de messagebox:
        - showerror | showinfo | showwarning
        - askquestion
        - askokcancel
        - askyesno
        - ... etc
"""
# 1. Importer messagebox de tkinter
# 2. def d'une focntion qui gere le pop-up
def showModalWindow():
    messagebox.showerror("ERROR","YOU'RE DOOMED !")


# widget button: fait appel à la fonction showModalWindow via l'option command

btn=tkinter.Button(app, text='I DARE YOU !', command=showModalWindow)


def yes_no():
    messagebox.askyesno("CHOOSE WISELY...")

btn1=tkinter.Button(app, text="Yes/No",command=yes_no)






#positionnement
case.pack()
curseur.pack()
bouton1.pack()
bouton2.pack()
spinBox.pack()
lb.pack()
btn.pack()
btn1.pack()


# Boucle
app.mainloop()