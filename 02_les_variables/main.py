#coding:utf-8

"""
    nommer une variable :   doit commencer par une lettre ou underscore
                            ne pas contenir de caractère spéciaux
                            ne pas contenir d'espaces

    types standards :       entiers numérique (int)
                            nombre flottant (float)
                            chaine de caractères (str)
                            booléen (bool)
    Les types standards sont des classes (attributs + méthodes)
"""
# > déclaration d'une variable typage dynamique
# Convention de nommage :camelCase

agePersonne = 28 # pas de ";"
prixHT = 120.4 # la valeur 120.4 sera interprété comme un float
_nom ="Mike"
continuer_partie = True # False

# > recup du type des variables
print ("TYPE DE VARIABLES : ")
print ("\t Type de la variable 'agePersonne' : ", type(agePersonne))
print ("\t Type de la variable 'prixHT' : ", type(prixHT))
print ("\t Type de la variable '_nom' : ", type(_nom))
print ("\t Type de la variable 'continuer_partie' : ", type(continuer_partie))

# > Affichage des valeurs des variables

print ("\nVALEURS DE VARIABLES : ")
print ("\t Valeur de la variable 'agePersonne' : ", agePersonne)
print ("\t Valeur de la variable 'prixHT' : ", prixHT)
print ("\t Valeur de la variable '_nom' : ", _nom)
print ("\t Valeur de la variable 'continuer_partie' : ", continuer_partie)

# > Exemples de méthodes : format() de la classe 'str'
texte = "l'âge de la personne est {} et son nom est {}"
print ("\n",texte.format(agePersonne, _nom))

# > Pour la saisie clavier
print ("\n\tLA SAISIE AU CLAVIER")
nomJoueur = input ("Choisissez un nom de joueur : ")
print ("Bienvenue ",nomJoueur)

# > Caster les données (conversion) : int(), float(), str(), bool()
print ("\nCAST DONNEES")
prixHT = input("Entrer un prix hors taxes : ") # -> retourne un 'str'

# > cast(conversion) de la saisie en int
prixHT = int(prixHT)

prixTTC=prixHT*1.2
print ("\nLe prix TTC : {}".format(prixTTC))

# > valeur entière d'un booléen
print ("\n")
valeur_booleen = True
valeur_booleen = int (valeur_booleen)
print ("\t Valeur entière de 'True' :", valeur_booleen)

##>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
##>>>>>>> NB : OPérateur en Python >>>>>>>>>>>>>>>
##>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

"""
    -> Les opérateurs arithmétiques :
        +  :addition
        -  :soustraction
        /  :division
        // :division entière
        *  :multiplication
        %  :reste de la division
        ** :puissanceMoenborough

    -> Lincrémentation
        x = x + y : x += y
        x = x + y : x -= y
        x = x * y : x *= y
    
    -> Opérateurs de comparaison :
        ==      :égal à
        !=      :différent de
        <,>     :inférieur, supérieur
        <=,=>   :inférieur ou égal, supérieur ou égal

"""
