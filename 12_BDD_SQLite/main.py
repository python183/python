#coding:utf-8
import sqlite3

# > 1. Connexion à la bdd

connexion = sqlite3.connect("BDD/db_users.db")

# > 2. Récupération d'un curseur (instanciation d'un objet de la classe 'cursor' de sqlite3)
# Le curseur est un outil pour travailler avec les requêtes SQL
curseur = connexion.cursor()
print(type(curseur))


# > 3. Requête SELECT: liste des users dans la bdd
    # 3.1 defintion de la requete + execution de la requête

curseur.execute("SELECT * FROM users")

    # 3.2 Récup du résultat de la requête SELECT

usersList=curseur.fetchall()

    # 3.3 Affichage du résultat (le resultat est sous forme de liste)
print("Liste des users".center(50,"="))
print("\t",usersList)
print("\n")
print("Liste des users avec un for".center(70,"="))
for users in usersList:
    print(users)

print("\n")
print("Liste des nom de users".center(70,"="))
for util in usersList:
    print(util[1])



# > 4. Requête SELECT... WHERE:récup d'un user via son ID
    # 4.1 defintion du paramètre à passer à la requête
pUserID=(1,) # les param doivent être défini sous forme de tuples

    # 4.2 Défintion et execution de la requête SQL avec passage de paramètre
curseur.execute("SELECT * FROM users WHERE id_user=?",pUserID)

    # 4.3 Récup du résultat de la requête
userDB = curseur.fetchone()
print("\n")
    # 4.4 Affichage du résultat (sous forme de tableau)
print("L'ID 1, avec la requete SELECT...WHERE".center(70,"="))
print(userDB)

print("\nDétails du user à l'id 1:")
print("\t -ID: ", userDB[0])
print("\t -nom: ", userDB[1])
print("\t -level: ", userDB[2])
print("\n")

# > 5. Requête SELECT... WHERE:récup d'un user via son nom et son level dans la DB
pUserID=("Larry",15)
curseur.execute("SELECT * FROM users WHERE name=? AND level=?",pUserID)
print("L'ID 2, avec la requete SELECT...WHERE...AND".center(70,"="))
userDB = curseur.fetchone()
print(userDB)
print("\n")


pUserID=("George",15)
curseur.execute("SELECT * FROM users WHERE name=? AND level=?",pUserID)
print("requete SELECT...WHERE...AND sans résultat".center(70,"="))
print("requete: ",pUserID)
userDB = curseur.fetchone()
print(userDB)

# > 6. Requête INSERT INTO... : ajoute dans la DB
print("\n")
print("Ajout dans la base de données".center(70,"="))
    # 6.1 Récup d'une nouvelle instance du curseur
curseur = connexion.cursor()
    # 6.2 Définition d'un user à ajouter comme paramètre à la requête
newUser = (curseur.lastrowid, "Paul", 20)
        # 6.2.1 Avec plusieurs ID
newUsers = [(curseur.lastrowid,"Anna",35),(curseur.lastrowid,"Clément",40)]

    # 6.3 Definition et execution de la requête INSERT avec le passage de params
curseur.execute("INSERT INTO users VALUES(?,?,?)", newUser)
curseur.executemany("INSERT INTO users VALUES(?,?,?)", newUsers)
    # 6.4 Validation des changement avec un comit
connexion.commit()
    # 6.5 Affichage d'un message
print("\t INFO: utilisateur ajouter avec succès")

# > 7. Requete UPDATE ... : modifier un user dans le DB
print("\nModification d'un user :")

    # 7.1 recup du curseur
curseur=connexion.cursor()
    
    # 7.2 recup du user à l'id=1 pour la mise à jour
curseur.execute("SELECT * FROM users WHERE id_user=1")
print("\t - User avant MAJ",curseur.fetchone())

    # 7.3 Défintion d'un user à mettre à jour (à passer en param à la requête)
updateUser = ("Luc",60,1) #(name,lvl,id)

    # 7.4 Def et exec de la requête avec comme param le userà MAJ

curseur.execute("UPDATE users SET name=?, level=? WHERE id_user=?", updateUser)
print("\t - User après MAJ",curseur.fetchone())
    # 7.5 validation des changement ans la DB avec un commit
connexion.commit()



# 8. requete DELETE... Supprimer un utilisateur
delUser=("Anna","Clément","Paul")
curseur.execute("DELETE FROM users WHERE name=? OR name=? OR name=?",delUser)
connexion.commit()




#=========================================================#
#==================GESTION DES ERREURS====================#
#=========================================================#
# > Fermeture de la connexion
connexion.close()

try:
    connexion=sqlite3.connect("BDD/db_users.db")
    newUser = (curseur.lastrowid, "Paul", 20)
    newUsers = [(curseur.lastrowid,"Anna",35),(curseur.lastrowid,"Clément",40)]
    curseur.execute("INSERT INTO users VALUES(?,?,?)", newUser)
    connexion.commit()

except Exception as ex:
    print("ERROR ASSHOLE: {}".format(ex))
    #annulation de la transaction
    connexion.rollback()

finally:
    connexion.close()


