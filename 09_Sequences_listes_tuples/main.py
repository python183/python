#coding:utf-8

"""
    - Séquence: liste:  ensemble modifiable non ordoné.
                tuples: 
"""
#---------------------------------------#
#-------------- Listes -----------------#
#---------------------------------------#

print("LES LISTES".center(50,"-"))

# > 1. Déclaration et création d'une liste
# >> 1ere méthode
inventaire = list() #classe list
print(type(inventaire))

# >> 2eme méthode
inventaire = [2,5,75,"voiture"] # liste vide
print(inventaire)

# >> raccourcis pour la création
#liste initialisé avec un nombre d'éléments

inventaire=["arc"]*10
print(inventaire)

# Initialisation avec des valeurs croissantes

inventaire = range(20) # -> 0-19
print(inventaire[:])

# > 2. Parcourir une liste
# 2.1: while
print("\nAvec un while")
inventaire = range(20)
i=0
while i<len(inventaire): # len(inventaire: retourne la taille de la liste)
    print(inventaire[i])
    i+=1
# 2.2: for
print("\nAvec un for")
for valeur in inventaire:
    print(valeur)
# 2.3:
inventaire = ["arc","épée","bouclier","hache","dague","tunique"]
print("1er élément :",inventaire[0])
print("3eme élément :",inventaire[2])
print("liste des éléments: ",inventaire[:])
# Récup des 2 premiers éléments
print(inventaire[:2])
# Récup des 2 derniers éléments
print(inventaire[2:])
# Autres ...
print("l'élement 3 en partant de la fin :", inventaire[-3])
print("dernier élément de la liste :", inventaire[-1])
print("élément d'indice 2 jusqu'à 4 (4 exclut): ", inventaire[2:4])

# > 3. modif d'un d'une liste
print("\nmodif liste")
print(inventaire[:])
inventaire[2]="bouclier d'acier"
print("après modif\n",inventaire[:])

# modifier les 2 premiers éléments
inventaire[:2]=["bouclier d'acier","bouclier d'acier"]
inventaire[:2]=["bouclier d'acier"]*2
print(inventaire[:])

# modifier toute la liste

inventaire[:]=["tunique"]*len(inventaire)
print(inventaire[:])

# > 4. Faire une recherche dans la liste
# vérif la présence de l'élément "tunique"
if "tunique" in inventaire:
    print("je possède une tunique")
else:
    print("je n'ai pas de tunique")

# > 5. fonction pour manipuler les listes

# 5.1 ajouter un élément en fin de liste
inventaire=[]
inventaire.append("hache")
print(inventaire)
inventaire.append("arc")
print(inventaire)

# 5.2 Ajouter un élément à un indice
inventaire.insert(1,"potion de mana")
print(inventaire[:])

# 5.3 Supprilmer un élement
inventaire.remove("arc")
print(inventaire[:])

# 5.4 Suppression avec 'del'
del inventaire[0] # supprime l'élément à l'indice 0
print(inventaire[:])

# > 6. trier les éléments par ordre croissant avec 'sort', décroissant 'reverse'
print("\nTri: ")
inventaire = ["arc","épée","bouclier","hache","dague","tunique"]
print(inventaire[:])
inventaire.sort()
print(inventaire[:])
print("\nTri décroissant")
inventaire.reverse()
print(inventaire[:])

# tool : dir(<classe>): afficher toutes les fonctions de la classe 
print("\nfonction de la classe list")
print(dir(list))

#--------------------------------------------------#
#-------------- Tuples (n-uplets) -----------------#
#--------------------------------------------------#

print("LES TUPLES".center(50,"-"))

"""
    - un Tuple est immuable, une fois créé on ne peut pas le modifier.
    On peut rien ajouter ni supprimer
    --> Exemples : faires des constantes
"""
# > Déclaration
monTuple = ()
print(type(monTuple))

#NB: ajout d'une (,) dans le cas d'une initialisation d'une seule valeur
monTuple=(45,68,42)
monTuple1=45, # sans les () moins lisible
print (type(monTuple))
print(type(monTuple1))
print(monTuple)
print(monTuple1)

# Affichage d'un tuple
print("element indice 0: ",monTuple[0])

# > Avantage des tuples
print("\nAvantage: affectations multiples")
# Affectation multiple
    # Au lieu de faire :
nombre1=1
nombre2=25
    # Avec un tuple
nombre1, nombre2 = 78,25 #(nombre1, nombre2) = (78,25)
print(nombre1)
print(nombre2)
print("\nAvantage: retourner plusieurs valeurs dans une fonction: ")
# > Retourner plusieurs valeurs dans une fonction
# def de la fonction
def getPositionJouer():
    posX=145
    posY=90
    return(posX,posY)


#--------------------programme principal
coordX=0
coordY=0

print ("Position du joueur: ({}/{})".format(coordX,coordY))
(coordX,coordY)=getPositionJouer()

print ("Position du joueur: ({}/{})".format(coordX,coordY))

# NB : modification du tuple: affectation multiple
coordX=200
coordY=35
print ("Position du joueur: ({}/{})".format(coordX,coordY))