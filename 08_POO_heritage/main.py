#coding:utf-8

# Convention: aValeur : Valeur est un argument ('a')

#__________________________Classe mère(super classe)__________________________#
class Vehicule:
    #------------------------------------#
    #----------- Constructeur -----------#
    #------------------------------------#
    # Déclaration de 2 attributs
    def __init__(self, aNom, aEssence):
        self.nom = aNom
        self.essence = aEssence

    #--------------------------------#
    #----------- Méthodes -----------#
    #--------------------------------#
    def montrerVehicule(self):
        return self.nom
    
    def seDeplacer(self):
        print("le véhicule {} se déplace ....".format(self.nom))

#___________________classe fille: VOITURE hérite de véhicule___________________#

class Voiture(Vehicule):  #eq : class Voiture extends Vehicule
    #------------------------------------#
    #----------- Constructeur -----------#
    #------------------------------------#
    def __init__(self, aNom, aEssence, aPuissance):
        
        # appel du ctor de la classe mère pour intitialiser les valeurs de nom et essence
        Vehicule.__init__(self, aNom, aEssence)

        # appel du ctor de la classe fille pour 
        self.puissance = aPuissance

    #---------------------------------------#
    #-------Redéfintion de méthode ---------#
    #---------------------------------------#
    # redéfintion (réecriture) de la focntion seDéplacer de Véhicule
    def seDeplacer(self):
        print("Je roule...")

#___________________classe fille: AVION hérite de véhicule___________________#     

class Avion (Vehicule):
    #------------------------------------#
    #----------- Constructeur -----------#
    #------------------------------------#
    def __init__(self, aNom, aEssence, aMoteur):
        
        # appel du ctor de la classe mère pour intitialiser les valeurs de nom et essence
        Vehicule.__init__(self, aNom, aEssence)

        # appel du ctor de la classe fille pour 
        self.moteur = aMoteur
    
    #--------------------------------------#
    #------ Redéfintion de méthode --------#
    #--------------------------------------#
    # redéfintion (réecriture) de la focntion seDeplacer de Véhicule
    def seDeplacer(self):
        print("Je vole...")

"""-------------------------------------------------------------
-------------------- Héritage Multiple -------------------------
-------------------------------------------------------------"""

# une 1ere classe de base (super classe)
class Druide:
    #ctor
    def __init__(self):
        self.magie=4
        super().__init__()

# une 2eme classe de base (super classe)
class Voleur:
    def __init__(self):
        # Appel au constructeur de la classe suivante
        # dans la liste (__mro__)(object dans cet exemple)
        super().__init__()
        self.dexterite=7

# une classe fille (dérivée) double
class Heroes(Voleur, Druide):
    pass


#_______________________________________________________________________________________________________________________________#
"""---------------------------------------------"""
"""------------PROGRAMME PRINCIPALE-------------"""
"""---------------------------------------------"""

# Instance d'un objet voiture:
v1 = Voiture("VW", 90, 120)

# Appel des fonctions de la classe mère
v1.seDeplacer()

# Affichage des propriétés: voiture
print("puissance: ", v1.puissance," ch","\nmarque:", v1.nom)
v1.seDeplacer()
# Affichage des propriétés: avion
a1 = Avion("Boeing", 4000, "Turboréacteur")
print("\nMarque: ", a1.nom, "\nCapacité: ", a1.essence,"\nMotorisation: ",a1.moteur)
a1.seDeplacer()

#----- Héritage multiple -----#
"""
    fonctions utiles :
        isinstance(<objet>, <classe>):             vérifie qu'une objet de la classe renseignée
        issubclass(<classe_fille>,<classe_mère>):  vérifie qu'une classe est une fille d'une autre 
"""
print("\n\tHéritage multiple")
h = Heroes()
print("h est une instance de 'Voleur': ",isinstance(h,Voleur))
# h est une instance de 'Voleur':  True
print("h est une instance de 'Druide': ",isinstance(h,Druide))
# h est une instance de 'Druide':  True

## > Récupération de l'attribut 'magie' définit dans Druide 
print("\tDextérité (Voleur): ", h.dexterite)
print("\tmagie (Druide): ", h.magie) # erreur: AttributeError

"""
    - AttributeError: a un lien avec l'algo que python utilise lors des appels
    - L'algo: MRO (Method Resolution Order) [méthode d'ordre de résolution]
        -> Retourne la hiérarchie de lhéritage sous forme de liste
        -> la liste est accessible via l'attribut de classe __mro__
    -> Solution: super()
"""

print("\nliste mro: resolution des appels de membres: ")
print(Heroes.__mro__)