#coding:utf-8

"""
    - Le module 'TKinter' (TK interface) de python permet de créer des interface graphiques (GUI: Graphical User Interface)
    - tkinter: des composants graphiques (widgets): fenêtres, bouton, case à cocher, étiquettes, zone de texte...
    - autres modules graphiques: PyMol (visualisation de structures chimiques en 3D)
    - equivalent de tkinter:
        - java: swing (awt) - javaFx
        - microsoft: Windows forms - wpf
"""

# Import du module TKinter
import tkinter

# 1. Création de la fenêtre de base (main-window)

mainApp = tkinter.Tk() #Instanciation de l'objet fenêtre principale

    # 1.1 définition d'un titre de la fenêtre
mainApp.title("YOLO")

    # 1.2 défintion de la dimension
mainApp.geometry("800x600")
mainApp.geometry("800x600+50+100")
    
    # 1.3 défintion de la dimension (largeur x hauteur)
#mainApp.minsize(680,480)
#mainApp.maxsize(1280,720)

"""
## 1.2 def de la dimension (l x L)
mainApp.geometry("LargeurXHauteur+0px+0px")
    + (-): position relative de la fenêtre
    ex: +0+0 > 0px en X (horizontal vers la droite), 0px en Y (vertical vers le bas)

"""

# 

#mainApp.positionfrom("user")

# Boucle principale qui garde la fenêtre ouverte
mainApp.mainloop()
