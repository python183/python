#coding:utf-8

"""
    - création d'une classe: classe <nom_classe>
    - Convention de nommage: camelCase
"""
class Human:
    #--------------------------------------------------#
    #------- définition des attributs de classe -------#
    #--------------------------------------------------#
    """
    * Défintion du constructeur de la classe:
        - on utilise la fonction __init__()
        - un mot clef: 'self' (eq 'this' en java)
    * On ajoute des attributs à la classe à travers ou avec l'objet 'self'
    * Types de méthodes:
        - Méthode d'instance (standard):    fonctionne sur une instance (objet)
        - Méthode de classe:                fonctionne sur une classe 
        - Méthode statique:                 fonction indépendante mais liée à une classe
    """
    humains_crees = 0
    lieu_habitation="terre"

    def __init__(self, prenom, age):
        # Ajout de l'attribut prenom (la présence du '_' indique que la variable est privée (convention))
        self._prenom = prenom
        # Ajout de l'attribut age
        self._age = age
        print("\t-> Création d'un human: ",self)
        
        # recup de l'attibut de classe
        Human.humains_crees+=1

    #--------------------------------------------#
    #------- Encapsulation des attributs --------#
    #--------------------------------------------#

    # 2. Défintion du getter de age: _getage
    def _getage(self):
        return self._age
    
    def _getprenom(self):
        return self._prenom
    
    # 3. Défintion du setter de age: _setage
    def _setage(self,nouvelAge):
        self._age=nouvelAge

    def _setprenom(self,nouveauPrenom):
        self._prenom=nouveauPrenom

    # 1. Définition de la propriété age
    # > Défintion avec property(<getter>, <setter>, <deleter>, <helper>)
    age=property(_getage, _setage)
    prenom=property(_getprenom, _setprenom)

    #--------------------------------------------#
    #-------- def de méthode d'instance ---------#
    #--------------------------------------------#
    def changerPlanete(cls,nouvellePlanete):
        Human.lieu_habitation=nouvellePlanete

    def parler(self,message):
        print("{} a dit: {}".format(self.prenom, message))
    
    #--------------------------------------------#
    #-------- def de méthode de classe ----------#
    #--------------------------------------------#
    # désigner la méthode changerPlanete comme méthode de classe
    changer_planete = classmethod(changerPlanete)
    
    #--------------------------------------------#
    #-------- def de méthode statique  ----------#
    #--------------------------------------------#
    def definirHumain():
        print("L'humain est classé comme étant le dernier maillon de la chaîne alimentaire...")
    
    # désigner la méthode changerPlanete comme méthode statique
    definir_humain=staticmethod(definirHumain)

    
#--------------------------------------------#
#------- Programme principale (main) --------#
#--------------------------------------------#


print("Lancement du programme")

# > Création (instanciation) d'un objet de la classe human
# 1er objet
h1=Human("Chuck Norris","immortel") # > human(): appel du cronstructeur (__init__) de la classe 
print("- Le prenom de h1: {}".format(h1.prenom))
print("- L'âge de h1: {}".format(h1.age))
print("\tnombre d'humain: ",Human.humains_crees)

# Appel du setter de age
h1.age = 40 # > Appel de la propriété 'age' -> appel du _setage pour modif
print("- Nouvel âge de h1 ->{}".format(h1.age))

# Appel de la méthode d'instance parler()
h1.parler("Hello world !")

# 2eme objet
h2=Human("Dori",25)
print("- Le prenom de h2: {}".format(h2.prenom))
h2.prenom="Nemo"
print("- Nouvel prénom de h2 ->{}".format(h2.prenom))

print("- L'âge de h2: {}".format(h2.age))
print("\tnombre d'humain: ",Human.humains_crees)

# > Test de la méthode de classe
print("\n\tTest de la méthode de classe")
print("Planète actuelle: {}".format(Human.lieu_habitation))
print("...Changement de planète...")
    #  changement de planete
Human.changer_planete("Mars")
print("Planète actuelle: {}".format(Human.lieu_habitation))

# > Test de la méthode statique
print("\nTest de la méthode statique")
Human.definir_humain()