#coding:utf-8

"""
    - mot clé : def (definition)
"""

# > fonction 'direBonjour' sans paramètres
def direBonjour():
    print("Bonjour")

# je ne suis plus dans la fonction (plus de tabulation)

# > Appel de la fonction
direBonjour()

# > fonction 'dire' avec paramètres (arguments)
def dire(nomPersonne, messagePersonne):
    print("{}: {}".format(nomPersonne,messagePersonne))

# > Appel de la fonction
print("\n\tFonction dire")
dire("Mike", "Hello World")
dire("Ben","Bonjour")

# > Appel de la fonction dire() avec un seul paramètre
# dire("tom") --> TypeError: dire() missing 1 required positional argument: 'messagePersonne'

# > Fonction 'dire2()' avec des arguments par défaut
print("\n\tFonction dire2, avec paramètre par défaut")

def dire2(nomPersonne="tom", messagePersonne="hello",agePersonne=28):
    print("{} ({} ans): {}".format(nomPersonne, agePersonne, messagePersonne))

# > Appel de la fonction dire2()
dire2()
dire2("Mike","Hello World")

# > Appel de la fonction dire2(): sans paramètres nommés 
dire2("Hi","24","John")

# > Appel de la fonction dire2(): avec paramètres nommés 
dire2(messagePersonne="Hi",agePersonne="24",nomPersonne="John")

# > Déclaration avec des paramètres extensible (equivalent des var-args en java)
print("\n\tfonction avec des paramètres extensibles")

def showInventory(*listeProduits):
    print("\n\tVotre inventaire: \n")
    for produit in listeProduits:
        print("\t-",produit)

# > Appel de la fonction showInventory()
showInventory("Iphone","Samsung","Sony")
showInventory("Iphone","Samsung","Sony","Nokia","Huawei")

# > Fonction avec un type de retour
print ("\n\tFonction avec un retour")

def somme(nbr1,nbr2):
    ans=nbr1+nbr2
    return (ans) # eq: return nbr1+nbr2

print("45 + 22 = ",somme(45,22))

# > Autre exemple avec une condition
def comparerNombre (nbr1,nbr2):
    if nbr1>nbr2:
        return nbr1
    elif nbr1<nbr2:
        return nbr2
    else:
        print ("Egalité !")

print("\n",comparerNombre(45,90))

## > Les Fonctions 'Lambda'
print ("\n\tFonction lambda")

# > Def d'une fonction : pas de nom
# lambda:print("Bonjour :) !!") eq : def:print("Bonjour :) !!")
coucou=lambda:print("Bonjour :) !!")

# > Appel de la fonciton lambda via la variable
coucou()

# > Exemple : définir une foncion lambda pour le calcul du prix TTC avec un TVA 20%
print("\n\tcalcul du prix TTC avec un TVA 20%")

# Déclaration de la fonction qui prend en argument (paramètre) la var prixHT
calculPrixTTC=lambda prixHT: prixHT*1.2
print("prix HT : 345.99€ \nprix TTC : ",calculPrixTTC(345.99),"€")

# > Exemple : focntion lambda avec  plusieurs paramètres
print("\n\tfonction lambda avec plusieurs paramètres")
calculer = lambda a,b:a+b
print(calculer(12,26))