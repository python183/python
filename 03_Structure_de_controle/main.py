#coding:utf-8

#------------------------------------------------#
#------------Structure conditionnelle------------#
#------------------------------------------------#

# > IF
identifiant="toto"
motDePasse="123"

print("Interface de connexion")

userID = input("Entrer votre identifiant :")
userPassword = input ("Saisir votre mot de passe :")

# > Test du mdp 
"""
    - délimité le if avec (:)
    - tabulation : le contenu du if 
    - condition multiples : and
                            or
                            in/not in (DANS/PAS DANS)
"""
if userPassword == motDePasse:
    print ("\tAccess granted ",userID)

print ("je ne suis plus dans le if")

#   AND, les 2 conditions doivent être vrai

if userPassword == motDePasse and userID == identifiant:
    print ("\t(if-and) Access granted ",userID)

#   OR, au moins une des condtions doit être vrai

if userPassword == motDePasse or userID == identifiant:
    print ("\t(if-or) Access granted ",userID)

#   IN | NOT IN

print ("\n IF - IN | NOT IN")

lettre_hasard = "b"

if lettre_hasard in "aeiouy":
    print ("c'est une voyelle")
if lettre_hasard not in "aeiouy":
    print ("c'est une consonne")

#   IF - ELSE

if lettre_hasard in "aeiouy":
    print ("c'est une voyelle")
else:
    print ("c'est une consonne")

#   IF - ELIF - ELSE (équivalent de if, else if , else en java)

print ("\nIF - ELIF - ELSE")
age = 24

if age == 18:
    print("tu viens d'être majeur")
elif age == 50:
    print ("tu viens d'avoir 50 ans")
elif age == 60:
    print ("tu viens d'avoir 60 ans")
else:
    print ("Tu as ",age," ans")

#   IF avec un booleen et le mot 'NOT'

print ("\nIF avec un booleen et le mot 'NOT'")

jeuCharge = True # True=1
if jeuCharge:
    print ("jeu chargé")
else:
    print("jeu en cours de chargement")

jeuCharge = False # False=0
if not jeuCharge:
    print("le jeu est éteint")
else:
    print ("le jeu est lancé")

# IF avec des valeurs sur des fourchettes
print ("\nIF avec des valeurs sur des fourchettes")
age = input("votre âge ?")

age  = int(age)

if age > 0 and age <=100:
    print ("L'âge est valide")
else:
    print ("L'âge est incorrecte")

# age > 0 and age < 100     ==>     0 < age <=100

if 0 < age <=100:
    print ("L'âge est valide")
else:
    print ("L'âge est incorrecte")



#------------------------------------------------#
#------------Structure d'itération---------------#
#------------------------------------------------#

# > la boucle 'WHILE'

compteur = 0
while compteur < 5:
    print("je suis une phrase")
    compteur +=1

"""
    mots-clés :     break       : casse la boucle)
                    continue    :revient au début de la boucle
"""
print ("\nboucles while avec 'break' et 'continue'")

jeuLance=True

while jeuLance :
    # instruction en rapport avec le jeu
    choixMenu = input(">")

    if choixMenu == "again":
        continue
    elif choixMenu == "quit":
        break # jeuLance = False
    elif choixMenu == "Salut":
        print ("Hello World")
    else:
        print ("commande introuvable")

print ("A bientôt...")
print (jeuLance)

# > la boucle 'FOR'

print ("\nboucle for")

phrase = "Bienvenue au langage Python"

for lettre in phrase:
    print (lettre)

