#coding:utf-8

"""
    2 types de fichiers:
        - fichier texte: caractères (chiffre et lettre)
        - fichier binaires: binaires (une suite d'octet)
"""

# > ouverture du fichier donnees.txt

"""
    - modes d'ouvertures:
        - le mode 'r' (read)            : ouvrir le fichier en read only
        - le mode 'w' (write)           : écrire dans un fichier avec remplacement
        - le mode 'a' (ajout, append)   : écrire dans un fichier avec ajout en fin de fichier
        - le mode 'x'                   : écrire et lire dans un fichier
        - le mode 'r+'                  : écrire et lire dans un même fichier
"""

######## > 1.Lecture dans un fichier

fichier = open("data/donnees.txt","r") # instance de l'objet fichier de type 'file'
# N.B: erreur sur le chemin du fichier > FileNotFoundError

# lecture dans le fichier (une seule ligne) avec la fonction 'readline()'
print ("\tLecture des lignes: \n")
ligne=fichier.readline()
ligne=fichier.readline()
print (ligne)

# lecture dans le fichier avec la fonction 'read()'

print ("\tLecture de l'ensembles: \n")
chaine=fichier.read()
print("contenu du fichier: \n",chaine)

# lecture dans le fichier

# > fermeture du fichier 
fichier.close()
print("le fichier est de type :",type(fichier.closed))
# > Vérif si le fichier est fermé

if fichier.closed:
    print(">le fichier est fermé")
else:
    print(">le fichier est encore ouvert")


#=========== raccourci pour l'ouverture du fichier =============#

with open("data/donnees.txt","r") as fichier:
    # lecture
    contenu=fichier.read()
    
    # affichage
    print("contenu du fichier(synthaxe 'with as'): \n",contenu)

    #NB: pas besoin de fermer le fichier avec le close


######### 2. Ecriture dans un fichier

# > Création et ouverture du fichier 'donneesUser.txt' en mode write

with open("data/donneesUser.txt","w") as fic:
    
    # ecriture d'un entier dans le fichier
    nombre=15
    fic.write(str(nombre)) # ne prend que des types str
    # ecriture de chaine dans le fichier
    fic.write("\nje suis une phrase")
    fic.write("\nje suis une autre phrase")

"""
    Serialisation / deserialisation: 
        avec le module 'pickle'
"""

