#coding:utf-8
import tkinter
"""
    Le principe du widget:
        <nom_variable> = <nom_widget>(<widget_parent>,<params>...)
        NB: <widget_parent> widget dans lequel il va être inséré

"""



# fenêtre principale
app=tkinter.Tk()
app.geometry("800x600")

# Créationd'un widget 'Label' (étiquette)
labelWelcome = tkinter.Label(app, text="Bienvenue à TKinter")
# N.B: widget 'Message': affichage sur plusieurs lignes

# afficher un paramètre passé à un widget 'cget(nom_param)'
print(labelWelcome.cget("text"))
print(labelWelcome["text"])# dictionnaire

# Création d'un widget pour la saisie 'Entry'
# par défaut: 20 caractères (rendu visuel) > on change la taille avec width
entryName=tkinter.Entry(app, width='30', show='*') # 'show='*'', n'affichera que des '*'

# > Fonction à executer au clic du bouton
def afficherMessage():
    print("Hello World")

#Création d'un bouton widget bouton
buttonQuit=tkinter.Button(app, text='Ok', width='20', height='1',command=afficherMessage)

# Positionnement des widget
labelWelcome.pack()
entryName.pack()
buttonQuit.pack()

# > modifier le param 'text'
labelWelcome.config(text="Bienvenue")
# Positionnement du label
labelWelcome.pack()

# > boucle
app.mainloop()

