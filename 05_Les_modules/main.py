#coding:utf-8

"""
    - toutes les classes de python sont rangées dans des modules
        -> exemple de module: maths

    - Pour utiliser une fonction issue d'un import:
        <nom_du_module>.<nom_de_la_fonction>: math.sqrt, math.cos

    - Eviter de pointer le module pour remonter les fcts avec l'instruction:
        from <nom_module> import <nom_fct1>,<nom_fct2>: from math import sqrt, cos      ## Pour importer plusieurs fonctions
        from <nom_module> import *: from math import *                                  ## Pour importer toutes les fonctions
"""

# Calcul de la racine carrée avec la fonction sqrt() du module 'maths'
"""
Sans importer le module maths

        ans=sqrt(100), sans importer le module:
--> NameError: name 'sqrt' is not defined
"""

import math

print("\n\tUtilisation du module maths avec la fonction sqrt et cosinus:\n")

ans=math.sqrt(100)
print("racine carrée de 100: ",ans)

from math import sqrt, cos

ans=sqrt(25)
print("racine carrée de 25: ",ans)
ans=cos(30)
print("cos(30): ",ans)

# Importation et utilisation d'un module personnalisé
import modele.player # le "." remplace le "/", modele.player <=> modele/player

# Import avec un alias
import modele.player as joueur

# import avec l'instruction from <nom_module> import <fonction>
from modele import player

print ("\n\tImportation et utilisation d'un module personnalisé:\n")

modele.player.parler("Mike","Salut")
modele.player.bye()

joueur.parler("Jean-Claude","Hey !")
joueur.bye()

player.bye()