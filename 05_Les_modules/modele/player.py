#coding:utf-8

"""
    - module pour le joueur
    - module de type fichier (.py, de bas niveau) qui peut contenir des fonctions, des variables, des classes...
"""

# 1ère fonction du module
def parler(personnage, message):
    print("{}: {}".format(personnage,message))

# 2ème fonction du module
def bye():
    print("Au revoir !")
