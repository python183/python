#coding:utf-8

import tkinter, random

"""
    - afficher une image dans un canvas: creat_image(x,y,options...)
        -> La fonction retourne l'identifiant de l'item image
        -> options:
            - anchor:   par défaut 'center'
                        valeurs: n-s-e-w
            - image:    image à afficher
"""

# > 1. Main window

mainwindow=tkinter.Tk()
mainwindow.title("cible")

# > 3. Widget
    # > 3.1 défintion d'une image à insérer dans le canvas
photo = tkinter.PhotoImage(file="cible/tk_cible.gif")
impact = tkinter.PhotoImage(file="cible/tk_impact.gif")

    # > 3.2 création d'un canvas
largeur=550
hauteur=550
canvas=tkinter.Canvas(mainwindow,width=largeur,height=hauteur,bg='white')

    # > 3.4 affichage de l'image
item = canvas.create_image(0,0,anchor='nw',image=photo)
print("\t-> Image de fond (item: {}".format(item))


# 4. Défintion des fonctions

def tirCercle():
 
    x = random.randint(0, largeur)
    y = random.randint(0, hauteur)
    """
    r = 20
    item = canvas.create_oval(x-r,y-r,x+r,y+r, outline='black',fill='black')

    print(("creation du cercle (cercle : {})".format(item)))
    print(canvas.find_all)
    """
    item = canvas.create_image(x,y,image=impact)
    print(("creation du cercle (cercle : {})".format(item)))
    print(canvas.find_all)

    
def undoTir():
    if len(canvas.find_all())>1:
        item = canvas.find_all()[-1]
        # On efface le dernier cercle
        canvas.delete(item)
""" effacer tous les cercles"""
def EraseAll():
    while len(canvas.find_all())>1:
        undoTir()





boutonTirer=tkinter.Button(mainwindow, text="Tirer", command=tirCercle)

    # > 3.7 Création d'un bouton effacer
boutonEffacerDernierTir = tkinter.Button(mainwindow, text=' Effacer Dernier Tir', command=undoTir)

    # > 3.9 Création d'un bouton effacer tout
boutonEffacerTout = tkinter.Button(mainwindow, text=' Effacer tout', command=EraseAll)

    # > 3.10 Création d'un bouton Quitter

boutonQuit=tkinter.Button(mainwindow,text='Quit',command=mainwindow.destroy)

    # > 3.3 Positionnement du canvas
canvas.pack()

    # > 3.6 Position du bouton tirer
boutonTirer.pack(side='left',padx=10,pady=10)


    # > 3. Position du bouton 
        # 8.Effacer dernier tir
        # 10.Effacer tout
        # 12.bouton quitter
boutonEffacerDernierTir.pack(side='left',padx=10,pady=10)
boutonEffacerTout.pack(side='left',padx=10,pady=10)
boutonQuit.pack(side='left',padx=10,pady=10)


# 2. Boucle
mainwindow.mainloop()