#coding:utf-8

import tkinter
import random


"""
    Notes:
        - le module random: génération de nombre aléatoires
        - Dessiner un cercle avec la fonction canvas.create_ovale()
            Canvas.create_oval(x0,y0,x1,y1,options...)
                -> options: outline: couleur de la bordure (par défaut black)
                            fill:  couleur de remplissage (par défaut transparent)
                    
"""


# Main window
mainwindow = tkinter.Tk()

# > Widgets
# > 1. Ajout d'un canvas (zone graphique)
largeur=480
hauteur=320
canvas=tkinter.Canvas(mainwindow,width=largeur,height=hauteur,bg='white')


# > 9. Défintion des Fonction
    #9.1 dessinerCercle
def dessinerCercle():
    # dessine un cercle de centre (x,y) et de rayon 'r'
    x = random.randint(0,largeur)
    y = random.randint(0, hauteur)
    r = random.randint(10,100)
    colors  = ["red","green","blue","orange","purple","pink","yellow"]
    color = random.choice(colors)
    canvas.create_oval(x-r,y-r,x+r,y+r,outline=color,fill=color)

    #9.1  effacerCercle
def effacerCercle():
    canvas.delete("all")




# > 3. Ajout d'un bouton 'Go': bouton pour dessiner
boutonGo=tkinter.Button(mainwindow, text="Go",command=dessinerCercle)

# > 5. Bouton pour effacer 'Erase'
boutonErase=tkinter.Button(mainwindow, text="Effacer", command=effacerCercle)

# > 7. Ajout d'un bouton quitter
boutonQuit=tkinter.Button(mainwindow,text='Quit',command=mainwindow.destroy)



# > 2.Positionnement
canvas.pack(padx=5,pady=5)

# > 4. Positionnement du boutton Go
boutonGo.pack(side="left",padx=10,pady=10)

# > 6. Positionnement du boutton Erase
boutonErase.pack(side='left',padx=5,pady=5)

# > 8. Positionnement du bouton Quit
boutonQuit.pack(side='left',padx=5,pady=5)

# > Boucle
mainwindow.mainloop()